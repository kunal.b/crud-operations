var {MongoClient} = require('mongodb');
var url = "mongodb://localhost:27017/user_managements";
MongoClient.connect(url, function (err, user) {
    const db = user.db("user_managements");
    if (err) 
        throw err;

    var myquery = { first_name: "Amitabh" };

    db.collection("users").deleteOne(myquery, function (err, obj) {
        if (err) 
            throw err;

        console.log(obj.result + " record(s) deleted");
        user.close();
    });
});
