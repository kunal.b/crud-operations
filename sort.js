var {MongoClient} = require('mongodb');
var url = "mongodb://localhost:27017/user_managements";
MongoClient.connect(url, function (err, user) {
    const db = user.db("user_managements")
    if (err)
        throw err;

    var sortOrder = {_id: +1}

    db.collection("users").find().sort(sortOrder).toArray(function(err, res){
        if(err)
        throw err

        console.group(res)
        user.close()
    })
})