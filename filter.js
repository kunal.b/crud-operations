var {MongoClient} = require('mongodb');
var url = "mongodb://localhost:27017/user_managements";
MongoClient.connect(url, function (err, user) {
    const db = user.db("user_managements")
    if (err)
        throw err;
    var query = { first_name: "The Great" };
    db.collection("users").find(query).toArray(function (err, res) {
        if (err)
            throw err;

        console.log(res)
        user.close()
    })

})