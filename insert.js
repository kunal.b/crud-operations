var MongoClient = require("mongodb").MongoClient;
 var url = "mongodb://localhost:27017/user_managements";
 MongoClient.connect(url, function (err, client) {
   if (err) throw err;
   var myobj = [{
    "first_name": "Kunal",
    "last_name":"Nisht",
    "email":"kbisht@gmai.com",
    "company":"Valuebound",
    "phone":"123456789"
   },
   {
    "first_name": "Amitabh",
    "last_name":"Bacchan",
    "email":"xyz@abc",
    "company":"Valuebound",
    "phone":"123456789"
   },
   {
    "first_name": "Hritik",
    "last_name":"Roshan",
    "email":"xyz@abc",
    "company":"Valuebound",
    "phone":"123456789"
   },
   {
    "first_name": "The Great",
    "last_name":"Khali",
    "email":"xyz@abc",
    "company":"Valuebound",
    "phone":"123456789"
   },
   ];
   const db = client.db("user_managements");
   db.collection("users").insertMany(myobj, function (err, res) {
     if (err) throw err;
     console.log("Number of records inserted: " + res.insertedCount);
     client.close();
   });
 });
